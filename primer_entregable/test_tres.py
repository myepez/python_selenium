import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By


class TestTres(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    def test_tres_female(self):
        driver = self.driver
        driver.get("https://www.seleniumeasy.com/test/basic-radiobutton-demo.html")

        radio_button_male = driver.find_element_by_xpath(
            "//body/div[@id='easycont']/div[1]/div[2]/div[1]/div[2]/label[1]/input[1]"
        )
        radio_button_female = driver.find_element_by_xpath(
            "//body/div[@id='easycont']/div[1]/div[2]/div[1]/div[2]/label[2]/input[1]"
        )
        get_checked_button = driver.find_element_by_xpath(
            "//button[@id='buttoncheck']"
        )
        text_output = driver.find_element_by_xpath(
            "//body/div[@id='easycont']/div[1]/div[2]/div[1]/div[2]/p[3]"
        )

        radio_button_female.click()
        time.sleep(5)
        get_checked_button.click()
        time.sleep(5)
        self.assertRegex(text_output.text, "Radio button 'Female' is checked")

    def test_tres_male(self):
        driver = self.driver
        driver.get("https://www.seleniumeasy.com/test/basic-radiobutton-demo.html")

        radio_button_male = driver.find_element_by_xpath(
            "//body/div[@id='easycont']/div[1]/div[2]/div[1]/div[2]/label[1]/input[1]"
        )
        radio_button_female = driver.find_element_by_xpath(
            "//body/div[@id='easycont']/div[1]/div[2]/div[1]/div[2]/label[2]/input[1]"
        )
        get_checked_button = driver.find_element_by_xpath(
            "//button[@id='buttoncheck']"
        )
        text_output = driver.find_element_by_xpath(
            "//body/div[@id='easycont']/div[1]/div[2]/div[1]/div[2]/p[3]"
        )

        radio_button_male.click()
        time.sleep(5)
        get_checked_button.click()
        time.sleep(5)
        self.assertRegex(text_output.text, "Radio button 'Male' is checked")

if __name__ == '__main__':
    unittest.main()
