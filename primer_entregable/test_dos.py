import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By


class TestDos(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    def test_dos(self):
        driver = self.driver
        driver.get("https://www.seleniumeasy.com/test/basic-checkbox-demo.html")

        time.sleep(2)
        self.driver.find_element(By.CSS_SELECTOR, ".checkbox:nth-child(2)  #isAgeSelected").click()
        time.sleep(5)
        text_output = driver.find_element_by_xpath('//*[@id="txtAge"]')
        self.assertEqual('Success - Check box is checked', text_output.text)


if __name__ == '__main__':
    unittest.main()
