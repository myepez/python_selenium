import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By


class TestUno(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    def test_uno(self):
        driver = self.driver
        driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html")
        text_input = driver.find_element_by_xpath("//input[@id='user-message']")
        show_button = driver.find_element_by_xpath("//button[contains(text(),'Show Message')]")
        text_output = driver.find_element_by_xpath('//*[@id="display"]')

        self.driver.find_element(By.CSS_SELECTOR, ".at4-close").click()
        text_input.send_keys('Maria Yepez')
        time.sleep(5)
        show_button.click()
        time.sleep(5)
        self.assertEqual('Maria Yepez', text_output.text)

if __name__ == '__main__':
    unittest.main()