# Primer Entregable

Este proyecto es el primer entregable del curso Python - Selenium

## Descargar

Use el comando git clone para descargar la carpeta

```bash
git clone https://gitlab.com/myepez/python_selenium.git
```

## Uso
Se debe ejecutar comandos cmd:

1. Dentro de la carpeta primer_entregable, para crear el virtual env
```cmd
virtualenv venv
```
2. Para activar el virtual env creado en el paso anterior
```cmd
venv\Scripts\activate
```
3. Para instalar el reporte de dependencias (.package)
```cmd
pip install -r .package
```
4. Para listar las dependencias instaladas en el paso anterior
```cmd
pip freeze
```

El resultado debe ser:
```cmd
selenium==3.141.0
urllib3==1.26.5
```
Sino, por favor borrar la carpeta venv y volver a repetir los pasos.

## Documento de Evidencias
En el archivo word se visualiza capturas de los pasos 1 y 2 solicitados.
Adicionalmente se ve la captura de las pruebas realizadas de cada script(Python file)

## Contribucion
Para mejoras o correcciones, por favor abrir un nuevo branch, subir las actualizaciones y luego realizar el pull request.



## License
[MIT](https://choosealicense.com/licenses/mit/)
